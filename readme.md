## Lite knight

[Play on Itch.io](https://itch.io/jam/make-a-level/rate/579078)

You play as a blob in this dark platformer level.

Your abilities:

Move blob left and right by pressing <kbd>A</kbd> and <kbd>D</kbd>.
Blob jumps and double jumps by pressing <kbd>W</kbd>.
Being a blob, you can cling to walls by holding <kbd>A</kbd> or <kbd>D</kbd>.
Walk on walls while clinging with <kbd>W</kbd> or <kbd>S</kbd>.

### Credits

Made by Ethan and Joshua Redmond in approximately 10 hours.

Thanks to Kenney Assets for the 1-bit pack used for the spritesheet.

Sound effects:

https://opengameart.org/content/swishes-sound-pack
https://opengameart.org/content/8-bit-sound-effects-library
https://opengameart.org/content/512-sound-effects-8-bit-style
https://opengameart.org/content/8-bit-sound-effects-library
https://www.kenney.nl/assets/impact-sounds
https://opengameart.org/content/water-splashes
https://www.playonloop.com/2018-music-loops/follow-me/
https://opengameart.org/content/8-bit-sound-effects-library

Built with Phaser, TypeScript, and Sass.
