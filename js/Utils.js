var Utils = /** @class */ (function () {
    function Utils() {
    }
    // This is a simple function-shortcut to avoid using lengthy document.getElementById.
    Utils.byId = function (elementId) {
        return document.getElementById(elementId);
    };
    Utils.randBetween = function (min, max) {
        return Math.round(Math.random() * (max - min) + min);
    };
    Utils.isCoordInZone = function (coordObj, zoneObj) {
        return (coordObj['x'] > zoneObj['startX']
            &&
                coordObj['x'] < zoneObj['endX']
            &&
                coordObj['y'] > zoneObj['startY']
            &&
                coordObj['y'] < zoneObj['endY']);
    };
    Utils.distBetweenPoints = function (x1, y1, x2, y2) {
        var dx = x1 - x2;
        var dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    };
    Utils.fadeIn = function (spriteObj, durationNum, callbackFunc) {
        game.add.tween({
            targets: [spriteObj],
            ease: 'Linear',
            duration: durationNum,
            delay: 0,
            alpha: {
                getStart: function () {
                    return 0;
                },
                getEnd: function () {
                    return 1;
                },
            },
            onComplete: function () {
                callbackFunc();
            },
        });
    };
    Utils.fadeOut = function (spriteObj, durationNum, callbackFunc) {
        game.add.tween({
            targets: [spriteObj],
            ease: 'Linear',
            duration: durationNum,
            delay: 0,
            alpha: {
                getStart: function () {
                    return 1;
                },
                getEnd: function () {
                    return 0;
                },
            },
            onComplete: function () {
                callbackFunc();
            },
        });
    };
    Utils.time = function () {
        return new Date().getTime();
    };
    Utils.scaleFactorNum = 4;
    Utils.depthsArr = {
        'world': 1,
        'light': 2,
        'player': 3,
        'Water': 9,
        'Signs': 9,
        'Misc': 9,
        'Animation1': 9,
        'Animation2': 9,
        'particles': 10,
    };
    return Utils;
}());
//# sourceMappingURL=Utils.js.map