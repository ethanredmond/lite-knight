var Help = /** @class */ (function () {
    function Help() {
    }
    Help.init = function () {
        if (localStorage.getItem('hasHiddenLeftRight')) {
            Help.hasHiddenLeftRight = true;
        }
        if (localStorage.getItem('hasHiddenJump')) {
            Help.hasHiddenJump = true;
        }
        if (localStorage.getItem('hasHiddenDoubleJump')) {
            Help.hasHiddenDoubleJump = true;
        }
        if (localStorage.getItem('hasShownCrouch')) {
            Help.hasShownCrouch = true;
        }
        if (localStorage.getItem('hasHiddenCrouch')) {
            Help.hasHiddenCrouch = true;
        }
        if (localStorage.getItem('hasHiddenCrouchWalk')) {
            Help.hasHiddenCrouchWalk = true;
        }
        if (!Help.hasHiddenLeftRight) {
            Help.showMessage('helpMessageLeftRight');
        }
        else if (!Help.hasHiddenJump) {
            Help.showMessage('helpMessageJump');
        }
        else if (!Help.hasHiddenDoubleJump) {
            Help.showMessage('helpMessageDoubleJump');
        }
    };
    Help.leftRightPressed = function () {
        if (!Help.hasHiddenLeftRight) {
            Help.hasHiddenLeftRight = true;
            localStorage.setItem('hasHiddenLeftRight', '1');
            Help.hideAll(function () {
                if (!Help.hasHiddenJump) {
                    Help.showMessage('helpMessageJump');
                }
            });
        }
    };
    Help.jumpPressed = function () {
        if (!Help.hasHiddenJump) {
            Help.hasHiddenJump = true;
            localStorage.setItem('hasHiddenJump', '1');
            Help.hideAll(function () {
                if (!Help.hasHiddenDoubleJump) {
                    Help.showMessage('helpMessageDoubleJump');
                }
            });
        }
    };
    Help.doubleJumpPressed = function () {
        if (!Help.hasHiddenDoubleJump) {
            Help.hasHiddenDoubleJump = true;
            localStorage.setItem('hasHiddenDoubleJump', '1');
            Help.hideAll();
        }
    };
    Help.crouchPressed = function () {
        if (Help.hasShownCrouch && !Help.hasHiddenCrouch) {
            Help.hasHiddenCrouch = true;
            Help.hideAll(function () {
                if (!Help.hasHiddenCrouchWalk) {
                    Help.showMessage('helpMessageCrouchWalk');
                }
            });
        }
    };
    Help.crouchWalkPressed = function () {
        if (Help.hasShownCrouch && !Help.hasHiddenCrouchWalk) {
            Help.hasHiddenCrouchWalk = true;
            localStorage.setItem('hasHiddenCrouchWalk', '1');
            Help.hideAll();
        }
    };
    Help.showMessage = function (helpMessageId, lifespanMillis) {
        if (lifespanMillis === void 0) { lifespanMillis = null; }
        Help.hideAll(function () {
            if (Utils.byId(helpMessageId)) {
                $("#" + helpMessageId).fadeIn(Help.fadeInMillis);
                if (lifespanMillis) {
                    setTimeout(Help.hideAll, lifespanMillis);
                }
            }
        });
    };
    Help.hideAll = function (callbackFunc) {
        var $visibleHelpMessages = $('.helpMessage:visible');
        if ($visibleHelpMessages.length) {
            $visibleHelpMessages.fadeOut(Help.fadeOutMillis, callbackFunc);
        }
        else if (callbackFunc) {
            callbackFunc();
        }
    };
    Help.fadeInMillis = 500;
    Help.fadeOutMillis = 500;
    Help.hasHiddenLeftRight = false;
    Help.hasHiddenJump = false;
    Help.hasHiddenDoubleJump = false;
    Help.hasShownCrouch = false;
    Help.hasHiddenCrouch = false;
    Help.hasHiddenCrouchWalk = false;
    return Help;
}());
//# sourceMappingURL=Help.js.map