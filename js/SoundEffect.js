var SoundEffect = /** @class */ (function () {
    function SoundEffect() {
    }
    SoundEffect.preload = function () {
        for (var soundEffectKey in SoundEffect.effectsArr) {
            if (SoundEffect.effectsArr[soundEffectKey]['numFiles'] == 1) {
                game.load.audio(soundEffectKey, SoundEffect.effectsArr[soundEffectKey]['srcPath']);
            }
            else {
                for (var i = 1; i <= SoundEffect.effectsArr[soundEffectKey]['numFiles']; i++) {
                    var srcPath = SoundEffect.effectsArr[soundEffectKey]['srcPath'];
                    var insertIndex = srcPath.indexOf('.');
                    srcPath = srcPath.slice(0, insertIndex) + i + srcPath.slice(insertIndex);
                    game.load.audio(soundEffectKey + i, srcPath);
                }
            }
        }
    };
    SoundEffect.create = function () {
        for (var soundEffectKey in SoundEffect.effectsArr) {
            if (SoundEffect.effectsArr[soundEffectKey]['numFiles'] == 1) {
                SoundEffect.audioObjsArr[soundEffectKey] = game.sound.add(soundEffectKey, { 'volume': SoundEffect.effectsArr[soundEffectKey]['volume'], 'loop': SoundEffect.effectsArr[soundEffectKey]['loop'] });
            }
            else {
                for (var i = 1; i <= SoundEffect.effectsArr[soundEffectKey]['numFiles']; i++) {
                    SoundEffect.audioObjsArr[soundEffectKey + i] = game.sound.add(soundEffectKey + i, { 'volume': SoundEffect.effectsArr[soundEffectKey]['volume'], 'loop': SoundEffect.effectsArr[soundEffectKey]['loop'] });
                }
            }
        }
    };
    SoundEffect.play = function (soundEffectKey) {
        if (soundEffectKey in SoundEffect.effectsArr) {
            var audioKey = soundEffectKey;
            if (SoundEffect.effectsArr[soundEffectKey]['numFiles'] > 1) {
                audioKey += Utils.randBetween(1, SoundEffect.effectsArr[soundEffectKey]['numFiles']);
            }
            if (audioKey in SoundEffect.audioObjsArr) {
                SoundEffect.audioObjsArr[audioKey].stop();
                SoundEffect.audioObjsArr[audioKey].play();
                return SoundEffect.audioObjsArr[audioKey];
            }
            else {
                console.log("Audio key \"" + audioKey + "\" is invalid");
            }
        }
        else {
            console.log("SoundEffect key \"" + soundEffectKey + "\" is invalid");
        }
        return null;
    };
    SoundEffect.effectsArr = {
        // @formatter:off
        'jump': { 'volume': 0.4, 'loop': false, 'numFiles': 4, 'srcPath': 'assets/audio/jump.wav' },
        'doubleJump': { 'volume': 0.4, 'loop': false, 'numFiles': 4, 'srcPath': 'assets/audio/doubleJump.wav' },
        'death': { 'volume': 0.4, 'loop': false, 'numFiles': 2, 'srcPath': 'assets/audio/death.mp3' },
        'respawn': { 'volume': 0.4, 'loop': false, 'numFiles': 1, 'srcPath': 'assets/audio/respawn.wav' },
        'checkpoint': { 'volume': 0.4, 'loop': false, 'numFiles': 1, 'srcPath': 'assets/audio/checkpoint.mp3' },
        'footStep': { 'volume': 0.3, 'loop': false, 'numFiles': 5, 'srcPath': 'assets/audio/footStep.ogg' },
        'fall': { 'volume': 0.4, 'loop': false, 'numFiles': 1, 'srcPath': 'assets/audio/fall.ogg' },
        'splash': { 'volume': 0.6, 'loop': false, 'numFiles': 1, 'srcPath': 'assets/audio/splash.wav' },
        'swim': { 'volume': 0.1, 'loop': false, 'numFiles': 1, 'srcPath': 'assets/audio/splash.wav' },
        'backgroundMusic': { 'volume': 0.1, 'loop': true, 'numFiles': 1, 'srcPath': 'assets/audio/backgroundMusic.wav' },
        'win': { 'volume': 0.8, 'loop': false, 'numFiles': 1, 'srcPath': 'assets/audio/win.mp3' },
    };
    SoundEffect.audioObjsArr = {};
    return SoundEffect;
}());
//# sourceMappingURL=SoundEffect.js.map