class Help {

    static readonly fadeInMillis: number  = 500;
    static readonly fadeOutMillis: number = 500;

    static hasHiddenLeftRight: boolean  = false;
    static hasHiddenJump: boolean       = false;
    static hasHiddenDoubleJump: boolean = false;
    static hasShownCrouch: boolean      = false;
    static hasHiddenCrouch: boolean     = false;
    static hasHiddenCrouchWalk: boolean = false;

    static init(): void {
        if (localStorage.getItem('hasHiddenLeftRight')) {
            Help.hasHiddenLeftRight = true;
        }
        if (localStorage.getItem('hasHiddenJump')) {
            Help.hasHiddenJump = true;
        }
        if (localStorage.getItem('hasHiddenDoubleJump')) {
            Help.hasHiddenDoubleJump = true;
        }
        if (localStorage.getItem('hasShownCrouch')) {
            Help.hasShownCrouch = true;
        }
        if (localStorage.getItem('hasHiddenCrouch')) {
            Help.hasHiddenCrouch = true;
        }
        if (localStorage.getItem('hasHiddenCrouchWalk')) {
            Help.hasHiddenCrouchWalk = true;
        }

        if (!Help.hasHiddenLeftRight) {
            Help.showMessage('helpMessageLeftRight');
        } else if (!Help.hasHiddenJump) {
            Help.showMessage('helpMessageJump');
        } else if (!Help.hasHiddenDoubleJump) {
            Help.showMessage('helpMessageDoubleJump');
        }
    }

    static leftRightPressed(): void {
        if (!Help.hasHiddenLeftRight) {
            Help.hasHiddenLeftRight = true;
            localStorage.setItem('hasHiddenLeftRight', '1');
            Help.hideAll(function () {
                if (!Help.hasHiddenJump) {
                    Help.showMessage('helpMessageJump');
                }
            });
        }
    }

    static jumpPressed(): void {
        if (!Help.hasHiddenJump) {
            Help.hasHiddenJump = true;
            localStorage.setItem('hasHiddenJump', '1');
            Help.hideAll(function () {
                if (!Help.hasHiddenDoubleJump) {
                    Help.showMessage('helpMessageDoubleJump');
                }
            });
        }
    }

    static doubleJumpPressed(): void {
        if (!Help.hasHiddenDoubleJump) {
            Help.hasHiddenDoubleJump = true;
            localStorage.setItem('hasHiddenDoubleJump', '1');
            Help.hideAll();
        }
    }

    static crouchPressed(): void {
        if (Help.hasShownCrouch && !Help.hasHiddenCrouch) {
            Help.hasHiddenCrouch = true;
            Help.hideAll(function () {
                if (!Help.hasHiddenCrouchWalk) {
                    Help.showMessage('helpMessageCrouchWalk');
                }
            });
        }
    }

    static crouchWalkPressed(): void {
        if (Help.hasShownCrouch && !Help.hasHiddenCrouchWalk) {
            Help.hasHiddenCrouchWalk = true;
            localStorage.setItem('hasHiddenCrouchWalk', '1');
            Help.hideAll();
        }
    }

    static showMessage(helpMessageId: string, lifespanMillis: number = null): void {
        Help.hideAll(function () {
            if (Utils.byId(helpMessageId)) {
                $(`#${helpMessageId}`).fadeIn(Help.fadeInMillis);
                if (lifespanMillis) {
                    setTimeout(Help.hideAll, lifespanMillis);
                }
            }
        });
    }

    static hideAll(callbackFunc?: () => any): void {
        const $visibleHelpMessages = $('.helpMessage:visible');
        if ($visibleHelpMessages.length) {
            $visibleHelpMessages.fadeOut(Help.fadeOutMillis, callbackFunc);
        } else if (callbackFunc) {
            callbackFunc();
        }
    }
}
