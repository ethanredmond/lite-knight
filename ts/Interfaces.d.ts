interface Zone {
    startX: number;
    startY: number;
    endX: number;
    endY: number;
}

interface Coord {
    x: number;
    y: number;
}

type Direction = 'up' | 'down' | 'left' | 'right';
type SoundEffectKey = keyof typeof SoundEffect.effectsArr & string;
