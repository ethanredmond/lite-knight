class SoundEffect {

    static readonly effectsArr: { [effectKey: string]: { volume: number, loop: boolean, numFiles: number, srcPath: string } } = {
        // @formatter:off
        'jump':                     {'volume': 0.4, 'loop': false, 'numFiles':  4, 'srcPath': 'assets/audio/jump.wav'}, // https://opengameart.org/content/swishes-sound-pack
        'doubleJump':               {'volume': 0.4, 'loop': false, 'numFiles':  4, 'srcPath': 'assets/audio/doubleJump.wav'}, // https://opengameart.org/content/swishes-sound-pack
        'death':                    {'volume': 0.4, 'loop': false, 'numFiles':  2, 'srcPath': 'assets/audio/death.mp3'}, // https://opengameart.org/content/8-bit-sound-effects-library
        'respawn':                  {'volume': 0.4, 'loop': false, 'numFiles':  1, 'srcPath': 'assets/audio/respawn.wav'}, // https://opengameart.org/content/512-sound-effects-8-bit-style
        'checkpoint':               {'volume': 0.4, 'loop': false, 'numFiles':  1, 'srcPath': 'assets/audio/checkpoint.mp3'}, // https://opengameart.org/content/8-bit-sound-effects-library
        'footStep':                 {'volume': 0.3, 'loop': false, 'numFiles':  5, 'srcPath': 'assets/audio/footStep.ogg'}, // https://www.kenney.nl/assets/impact-sounds
        'fall':                     {'volume': 0.4, 'loop': false, 'numFiles':  1, 'srcPath': 'assets/audio/fall.ogg'}, // https://www.kenney.nl/assets/impact-sounds
        'splash':                   {'volume': 0.6, 'loop': false, 'numFiles':  1, 'srcPath': 'assets/audio/splash.wav'}, // https://opengameart.org/content/water-splashes
        'swim':                     {'volume': 0.1, 'loop': false, 'numFiles':  1, 'srcPath': 'assets/audio/splash.wav'}, // https://opengameart.org/content/water-splashes
        'backgroundMusic':          {'volume': 0.1, 'loop': true,  'numFiles':  1, 'srcPath': 'assets/audio/backgroundMusic.wav'}, // https://www.playonloop.com/2018-music-loops/follow-me/
        'win':                      {'volume': 0.8, 'loop': false, 'numFiles':  1, 'srcPath': 'assets/audio/win.mp3'}, // https://opengameart.org/content/8-bit-sound-effects-library
        // @formatter:on
    };
    static audioObjsArr: { [soundEffectKey in SoundEffectKey]: Phaser.Sound.HTML5AudioSound } = {};

    static preload() {
        for (let soundEffectKey in SoundEffect.effectsArr) {
            if (SoundEffect.effectsArr[soundEffectKey]['numFiles'] == 1) {
                game.load.audio(soundEffectKey, SoundEffect.effectsArr[soundEffectKey]['srcPath']);
            } else {
                for (let i = 1; i <= SoundEffect.effectsArr[soundEffectKey]['numFiles']; i++) {
                    let srcPath = SoundEffect.effectsArr[soundEffectKey]['srcPath'];
                    const insertIndex = srcPath.indexOf('.');
                    srcPath = srcPath.slice(0, insertIndex) + i + srcPath.slice(insertIndex);
                    game.load.audio(soundEffectKey + i, srcPath);
                }
            }
        }
    }

    static create() {
        for (let soundEffectKey in SoundEffect.effectsArr) {
            if (SoundEffect.effectsArr[soundEffectKey]['numFiles'] == 1) {
                SoundEffect.audioObjsArr[soundEffectKey] = <Phaser.Sound.HTML5AudioSound>game.sound.add(soundEffectKey, {'volume': SoundEffect.effectsArr[soundEffectKey]['volume'], 'loop': SoundEffect.effectsArr[soundEffectKey]['loop']});
            } else {
                for (let i = 1; i <= SoundEffect.effectsArr[soundEffectKey]['numFiles']; i++) {
                    SoundEffect.audioObjsArr[soundEffectKey + i] = <Phaser.Sound.HTML5AudioSound>game.sound.add(soundEffectKey + i, {'volume': SoundEffect.effectsArr[soundEffectKey]['volume'], 'loop': SoundEffect.effectsArr[soundEffectKey]['loop']});
                }
            }
        }
    }

    static play(soundEffectKey: SoundEffectKey): Phaser.Sound.HTML5AudioSound | null {
        if (soundEffectKey in SoundEffect.effectsArr) {
            let audioKey = soundEffectKey;
            if (SoundEffect.effectsArr[soundEffectKey]['numFiles'] > 1) {
                audioKey += Utils.randBetween(1, SoundEffect.effectsArr[soundEffectKey]['numFiles']);
            }
            if (audioKey in SoundEffect.audioObjsArr) {
                SoundEffect.audioObjsArr[audioKey].stop();
                SoundEffect.audioObjsArr[audioKey].play();
                return SoundEffect.audioObjsArr[audioKey];
            } else {
                console.log("Audio key \"" + audioKey + "\" is invalid");
            }
        } else {
            console.log("SoundEffect key \"" + soundEffectKey + "\" is invalid");
        }
        return null;
    }

}
