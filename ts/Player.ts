class Player {

    static isCrouching                      = false;
    static isJumping                        = false;
    static isAbleToDoubleJump               = true;
    static isAbleToCrouch                   = true;
    static isStillHoldingCrouch             = false;
    static stillHoldingCrouchKey: Direction = null;
    static crouchDirection: Direction       = null;
    static hasReleasedJumpKey               = false;
    static isAlive                          = true;
    static isFalling                        = true;
    static moveSoundLengthCounter           = 0;
    static isPlayingWaterSplash             = false;
    static isInWater                        = false;
    static isInWaterTimeout: number         = null;
    static movmentParticlesTypeStr          = '';
    static redButtonUnPressedColliderObj: Phaser.Physics.Arcade.Collider;

    static readonly playerVelocityX      = 500;
    static readonly playerVelocityY      = 500;
    static readonly playerClimbVelocityY = 400;
    static readonly playerDragX          = 200;
    static readonly playerAccelerationX  = 100;
    static readonly lightTickNum         = 3;
    static playerLightCircleRadius       = 750;

    static spriteObj: Phaser.Physics.Arcade.Sprite;

    static cursorKeysArr: { [keyName: string]: Phaser.Input.Keyboard.Key };
    static wasdKeysArr: { [keyName: string]: Phaser.Input.Keyboard.Key };
    static moveKeysArr: { [keyName: string]: boolean };
    static checkpointNum: number  = 0;
    static spawnPointObj: Coord;
    static deathZonesArr: Zone[]  = [];
    static checkPointsArr: Zone[] = [];
    static waterZonesArr: Zone[]  = [];
    static crouchHelpZone: Zone[] = [];
    static redButtonZoneObj: Zone;

    static init() {
        const savedCheckpointNum = localStorage.getItem('checkpointNum');
        if (savedCheckpointNum) {
            Player.checkpointNum = parseInt(savedCheckpointNum);
        }
        Player.setSpawnPoint(true);
        Player.spriteObj       = game.physics.add.sprite(Player.spawnPointObj.x, Player.spawnPointObj.y, 'player');
        Player.spriteObj.depth = Utils.depthsArr['player'];
        Player.spriteObj.setSize(12, 16);
        Player.spriteObj.setScale(Utils.scaleFactorNum);
        game.physics.add.collider(Player.spriteObj, World.collideLayer);
        game.physics.add.collider(Player.spriteObj, World.redButtonPressedLayer);
        Player.redButtonUnPressedColliderObj = game.physics.add.collider(Player.spriteObj, World.redButtonUnPressedLayer);
        game.cameras.main.startFollow(Player.spriteObj, true, 0.3, 0.3);
        game.cameras.main.setBackgroundColor('#472d3c');

        Player.cursorKeysArr = game.input.keyboard.createCursorKeys();
        Player.wasdKeysArr   = {
            'up':      game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W),
            'upSpace': game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE),
            'left':    game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A),
            'down':    game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S),
            'right':   game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D),
        };

        Player.createAnimations();
    }

    static setSpawnPoint(isFromInit: boolean) {
        const checkpointZoneObj = Player.checkPointsArr[Player.checkpointNum];
        Player.spawnPointObj    = {
            'x': checkpointZoneObj.startX + ((checkpointZoneObj.endX - checkpointZoneObj.startX) / 2),
            'y': checkpointZoneObj.startY + ((checkpointZoneObj.endY - checkpointZoneObj.startY) / 2),
        };
        if (!isFromInit) {
            World.checkpointEmitter.explode(12, checkpointZoneObj.startX + (checkpointZoneObj.endX - checkpointZoneObj.startX) / 2, checkpointZoneObj.endY);
        }
    }

    static createAnimations() {
        game.anims.create({
            key:       'idle',
            frames:    game.anims.generateFrameNumbers('player', {start: 0, end: 1}),
            frameRate: 0.75,
            repeat:    -1,
        });

        game.anims.create({
            key:       'move',
            frames:    game.anims.generateFrameNumbers('player', {start: 1, end: 3}),
            frameRate: 10,
            repeat:    -1,
        });

        game.anims.create({
            key:       'jump',
            frames:    [{key: 'player', frame: 4}],
            frameRate: 10,
        });

        game.anims.create({
            key:       'fall',
            frames:    [{key: 'player', frame: 5}],
            frameRate: 10,
        });

        game.anims.create({
            key:       'crouch',
            frames:    game.anims.generateFrameNumbers('player', {start: 6, end: 7}),
            frameRate: 0.75,
            repeat:    -1,
        });
    }

    static update() {
        if (Player.isAlive) {
            // Base move keys.
            Player.moveKeysArr = {
                // @formatter:off
                'up':    (Player.cursorKeysArr.up.isDown    || Player.wasdKeysArr.up.isDown || Player.wasdKeysArr.upSpace.isDown),
                'down':  (Player.cursorKeysArr.down.isDown  || Player.wasdKeysArr.down.isDown),
                'left':  (Player.cursorKeysArr.left.isDown  || Player.wasdKeysArr.left.isDown),
                'right': (Player.cursorKeysArr.right.isDown || Player.wasdKeysArr.right.isDown),
                // @formatter:on
            };

            // Is still holding crouch.
            if (Player.isStillHoldingCrouch) {
                Player.isStillHoldingCrouch = Player.moveKeysArr[Player.stillHoldingCrouchKey];
            }
            // End crouch if has released key.
            if (Player.isCrouching) {
                if (Player.spriteObj.angle == 90 && (!Player.moveKeysArr['left'] || Player.moveKeysArr['right'])) {
                    Player.isCrouching     = false;
                    Player.crouchDirection = null;
                } else if (Player.spriteObj.angle == -90 && (!Player.moveKeysArr['right'] || Player.moveKeysArr['left'])) {
                    Player.isCrouching     = false;
                    Player.crouchDirection = null;
                } else if (Player.spriteObj.angle == 180 && !Player.moveKeysArr['up']) {
                    Player.isCrouching     = false;
                    Player.crouchDirection = null;
                }
            }

            // Jump key released.
            if (!Player.spriteObj.body.blocked.down) {
                if (!Player.hasReleasedJumpKey) {
                    Player.hasReleasedJumpKey = !Player.moveKeysArr['up'];
                }
            } else {
                Player.hasReleasedJumpKey = false;
            }
            if (Player.isJumping && !Player.spriteObj.body.blocked.none) {
                Player.isJumping = false;
            }
            // Jump.
            if (
                Player.moveKeysArr['up']
                &&
                !Player.isCrouching
                &&
                (
                    Player.spriteObj.body.blocked.down
                    // ||
                    // Player.isCrouching
                    ||
                    (
                        // Double jump.
                        Player.hasReleasedJumpKey
                        &&
                        !Player.spriteObj.body.blocked.down // Isn't touching downwards.
                        &&
                        Player.isAbleToDoubleJump
                    )
                )
            ) {
                if (
                    Player.hasReleasedJumpKey
                    &&
                    !Player.spriteObj.body.blocked.down // Isn't touching downwards.
                    &&
                    Player.isAbleToDoubleJump
                ) {
                    Help.doubleJumpPressed();
                } else {
                    Help.jumpPressed();
                }
                if (Player.isAbleToDoubleJump && !Player.spriteObj.body.blocked.down) {
                    SoundEffect.play('doubleJump');
                    Player.movmentParticlesTypeStr = 'jump';
                    setTimeout(function () {
                        Player.movmentParticlesTypeStr = '';
                    }, 75);
                } else {
                    SoundEffect.play('jump');
                    Player.movmentParticlesTypeStr = 'jump';
                    setTimeout(function () {
                        Player.movmentParticlesTypeStr = '';
                    }, 50);
                }
                Player.isAbleToDoubleJump        = (Player.spriteObj.body.blocked.down);
                // Player.isAbleToDoubleJump        = (!Player.spriteObj.body.blocked.none || Player.isCrouching);
                Player.isCrouching               = false;
                Player.crouchDirection           = null;
                Player.isJumping                 = true;
                Player.hasReleasedJumpKey        = false;
                Player.spriteObj.body.velocity.y = -Player.playerVelocityY;
                Player.spriteObj.anims.play('jump');
            }
            // Crouch on ceiling.
            // if (Player.isJumping) {
            //     if (Player.spriteObj.body.blocked.up && !Player.isStillHoldingCrouch && Player.isAbleToCrouch) { // Don't re-crouch if already crouched while holding key.
            //         Player.isStillHoldingCrouch  = true;
            //         Player.stillHoldingCrouchKey = 'up';
            //         Player.spriteObj.anims.play('crouch', true);
            //         Player.isCrouching = true;
            //     }
            // }

            // Crouch.
            if (Player.isCrouching) {
                // @ts-ignore
                Player.spriteObj.body.setAllowGravity(false);
                Player.spriteObj.setVelocityY(0);
                if (Player.moveKeysArr['left']) {
                    Player.spriteObj.body.velocity.x = -Player.playerVelocityX;
                } else if (Player.moveKeysArr['right']) {
                    Player.spriteObj.body.velocity.x = Player.playerVelocityX;
                }
                if (Player.spriteObj.body.blocked.left) {
                    Player.spriteObj.angle = 90;
                } else if (Player.spriteObj.body.blocked.right) {
                    Player.spriteObj.angle = -90;
                }

                if (Player.moveKeysArr['up']) {
                    Player.spriteObj.body.velocity.y = -Player.playerClimbVelocityY;
                    if (!Player.spriteObj.body.blocked[Player.crouchDirection]) {
                        Player.isCrouching     = false;
                        Player.crouchDirection = null;
                    }
                    Player.spriteObj.anims.play('move', true);
                    Player.spriteObj.flipX = (Player.crouchDirection == 'left');
                    Help.crouchWalkPressed();
                    Player.playMoveSound('climb', 15);
                } else if (Player.moveKeysArr['down']) {
                    Player.spriteObj.body.velocity.y = Player.playerClimbVelocityY;
                    if (!Player.spriteObj.body.blocked[Player.crouchDirection]) {
                        Player.isCrouching     = false;
                        Player.crouchDirection = null;
                    }
                    Player.spriteObj.anims.play('move', true);
                    Player.spriteObj.flipX = (Player.crouchDirection == 'right');
                    Help.crouchWalkPressed();
                    Player.playMoveSound('climb', 15);
                } else if (Player.crouchDirection == 'left' && Player.moveKeysArr['right']) {
                    // Crouch jump.
                } else {
                    Player.spriteObj.anims.play('idle', true);
                }
            } else {
                // @ts-ignore
                Player.spriteObj.body.setAllowGravity(true);
                Player.spriteObj.angle = 0;

                if (Player.spriteObj.body.blocked.down && (Player.moveKeysArr['left'] || Player.moveKeysArr['right'])) {
                    Player.playMoveSound('footStep', 15);
                }

                // if (Player.isInWater && (Player.moveKeysArr['left'] || Player.moveKeysArr['right'])) {
                //     Player.playMoveSound('swim', 25);
                // }

                // Move left.
                if (Player.moveKeysArr['left']) {
                    Player.spriteObj.body.velocity.x = Math.max(Math.min(Player.spriteObj.body.velocity.x, 0) - Player.playerAccelerationX, -Player.playerVelocityX);
                    Player.spriteObj.anims.play('move', true);
                    Player.spriteObj.flipX = true;
                    Help.leftRightPressed();

                    if (Player.spriteObj.body.blocked.left/* && !Player.isStillHoldingCrouch*/) { // Don't re-crouch if already crouched while holding key.
                        Player.isStillHoldingCrouch  = true;
                        Player.stillHoldingCrouchKey = 'left';
                        Player.crouchDirection       = 'left';
                        Player.spriteObj.anims.play('crouch', true);
                        Player.isCrouching = true;
                        SoundEffect.play('fall');
                        Help.crouchPressed();
                    }

                    // Move right.
                } else if (Player.moveKeysArr['right']) {
                    Player.spriteObj.body.velocity.x = Math.min(Math.max(Player.spriteObj.body.velocity.x, 0) + Player.playerAccelerationX, Player.playerVelocityX);
                    Player.spriteObj.anims.play('move', true);
                    Player.spriteObj.flipX = false;
                    Help.leftRightPressed();

                    if (Player.spriteObj.body.blocked.right/* && !Player.isStillHoldingCrouch*/) { // Don't re-crouch if already crouched while holding key.
                        Player.isStillHoldingCrouch  = true;
                        Player.stillHoldingCrouchKey = 'right';
                        Player.crouchDirection       = 'right';
                        Player.spriteObj.anims.play('crouch', true);
                        Player.isCrouching = true;
                        SoundEffect.play('fall');
                        Help.crouchPressed();
                    }

                    // Idle.
                } else {
                    Player.spriteObj.body.velocity.x = Math.max(Player.spriteObj.body.velocity.x - Player.playerDragX, 0);
                    if (Player.spriteObj.body.blocked.down) {
                        Player.spriteObj.anims.play('idle', true);
                    } else if (Player.spriteObj.body.velocity.y < 0) {
                        Player.spriteObj.anims.play('jump');
                    } else if (Player.spriteObj.body.velocity.y > 0) {
                        Player.spriteObj.anims.play('fall');
                    }
                }
                const wasFalling = Player.isFalling;
                Player.isFalling = (Player.spriteObj.body.velocity.y > 0);
                if (wasFalling && !Player.isFalling && Player.spriteObj.body.blocked.down) {
                    SoundEffect.play('fall');
                    Player.movmentParticlesTypeStr = 'fall';
                    setTimeout(function () {
                        Player.movmentParticlesTypeStr = '';
                    }, 100);
                }
            }
            for (let i = 0; i < World.signsArr.length; i++) {
                if (Utils.distBetweenPoints(Player.spriteObj.x, Player.spriteObj.y, World.signsArr[i].x, World.signsArr[i].y) < 80) {
                    if (!World.signsArr[i]['isShowing']) {
                        World.signsArr[i]['textObj'] = game.add.text(World.signsArr[i].x, World.signsArr[i].y - 45, World.signsArr[i].text, {
                            'fontFamily':      'textFont',
                            'align':           'center',
                            'backgroundColor': 'rgba(50, 47, 46, 0.7)',
                            'padding':         {
                                'left':   8,
                                'right':  8,
                                'top':    5,
                                'bottom': 5,
                            },
                            'style':           {
                                'borderRadius': '10px',
                            },
                        });
                        World.signsArr[i]['textObj'].setOrigin(0.5, 1);
                        World.signsArr[i]['isShowing']     = true;
                        World.signsArr[i]['textObj'].alpha = 0;
                        World.signsArr[i]['textObj'].depth = Utils.depthsArr['Signs'];
                        Utils.fadeIn(World.signsArr[i]['textObj'], 100, function () {
                        });
                    }
                } else if (World.signsArr[i]['isShowing']) {
                    if (World.signsArr[i]['textObj']) {
                        Utils.fadeOut(World.signsArr[i]['textObj'], 100, function () {
                            World.signsArr[i]['textObj'].destroy();
                        });
                    }
                    World.signsArr[i]['isShowing'] = false;
                }
            }
        } else {
            Player.spriteObj.setVelocityX(0);
            Player.spriteObj.setVelocityY(0);
        }
        if (Player.movmentParticlesTypeStr) {
            var particleAmountNum = 2;
            if (Player.movmentParticlesTypeStr == 'footStep' || Player.movmentParticlesTypeStr == 'climb') {
                particleAmountNum = 1;
            }
            for (var i = 0; i < particleAmountNum; i++) {
                if (Player.spriteObj.body.velocity.x < 10) {
                    var xPos = Player.spriteObj.x + ((Math.random() * 35) - 17.5);
                } else {
                    var xPos = Player.spriteObj.x + ((Math.random() * 10) - 5);
                }
                if (Player.movmentParticlesTypeStr == 'fall') {
                    var yPos = Player.spriteObj.y + ((Math.random() * 2) - 1) + 30;
                } else {
                    var yPos = Player.spriteObj.y + ((Math.random() * 10) - 5) + 30;
                }
                if (Math.random() > 0.5) {
                    World.movement1Emitter.explode(1, xPos, yPos);
                } else {
                    World.movement2Emitter.explode(1, xPos, yPos);
                }
            }
        }

        Player.isAbleToCrouch = true;
        Player.checkIfInDeathZone();
        Player.checkIfInCheckpointZone();
        Player.checkIfInWaterZone();
        Player.checkIfInCrouchHelpZone();
        Player.checkIfInRedButtonZone();
    }

    static playMoveSound(particleTypeStr: string, intervalNum: number) {
        if (this.moveSoundLengthCounter >= intervalNum) {
            SoundEffect.play('footStep');
            Player.movmentParticlesTypeStr = particleTypeStr;
            setTimeout(function () {
                Player.movmentParticlesTypeStr = '';
            }, 5);
            this.moveSoundLengthCounter = 0;
        }
        this.moveSoundLengthCounter++;
    }

    static kill() {
        if (Player.isAlive) {
            Player.isAlive = false;
            game.cameras.main.shake(200, 0.003);
            World.deathEmitter.explode(12, Player.spriteObj.x, Player.spriteObj.y + 20);
            SoundEffect.play('death');
            Utils.fadeOut(Player.spriteObj, 100,
                function () {
                    setTimeout(function () {
                        Player.respawn();
                    }, 300);
                });
            Help.showMessage('helpMessageDied', 1000);
        }
    }

    static respawn() {
        Player.spriteObj.x = Player.spawnPointObj.x;
        Player.spriteObj.y = Player.spawnPointObj.y;
        setTimeout(function () {
            SoundEffect.play('respawn');
            World.deathEmitter.explode(12, Player.spriteObj.x, Player.spriteObj.y + 10);
            Player.spriteObj.alpha = 1;
            game.add.tween({
                targets:    [Player.spriteObj],
                ease:       'Sine.easeInOut',
                duration:   300,
                delay:      0,
                scale:      {
                    getStart: function () {
                        return Utils.scaleFactorNum / 4;
                    },
                    getEnd:   function () {
                        return Utils.scaleFactorNum;
                    },
                },
                onComplete: function () {
                    Player.isAlive = true;
                },
            });
        }, 500);
    }

    static checkIfInDeathZone() {
        for (let j = 0; j < Player.deathZonesArr.length; j++) {
            if (Utils.isCoordInZone(Player.spriteObj, Player.deathZonesArr[j])) {
                Player.kill();
                break;
            }
        }
    }

    static checkIfInCheckpointZone() {
        for (let j = 0; j < Player.checkPointsArr.length; j++) {
            if (Player.checkpointNum != j && j > Player.checkpointNum && Utils.isCoordInZone(Player.spriteObj, Player.checkPointsArr[j])) {
                Player.checkpointNum = j;
                localStorage.setItem('checkpointNum', '' + Player.checkpointNum);
                SoundEffect.play('checkpoint');
                Player.setSpawnPoint(false);
                break;
            }
        }
    }

    static checkIfInWaterZone() {
        for (let j = 0; j < Player.waterZonesArr.length; j++) {
            if (Utils.isCoordInZone(Player.spriteObj, Player.waterZonesArr[j])) {
                if (Player.spriteObj.body.velocity.y > 400 && !Player.isPlayingWaterSplash) {
                    Player.isPlayingWaterSplash = true;
                    SoundEffect.play('splash');
                    setTimeout(function () {
                        Player.isPlayingWaterSplash = false;
                    }, 1000);
                }
                Player.isInWater = true;
                clearTimeout(Player.isInWaterTimeout);
                Player.isInWaterTimeout          = setTimeout(function () {
                    Player.isInWater = false;
                }, 400);
                Player.spriteObj.body.velocity.y = Math.max(Player.spriteObj.body.velocity.y - 50, -Player.playerVelocityY / 2);
                Player.isAbleToCrouch            = false;
                break;
            }
        }
    }

    static checkIfInCrouchHelpZone() {
        if (Player.isAbleToCrouch) {
            for (let j = 0; j < Player.crouchHelpZone.length; j++) {
                if (Utils.isCoordInZone(Player.spriteObj, Player.crouchHelpZone[j]) && !Help.hasShownCrouch) {
                    Help.hasShownCrouch = true;
                    localStorage.setItem('hasShownCrouch', '1');
                    Help.showMessage('helpMessageCrouch');
                    break;
                }
            }
        }
    }

    static checkIfInRedButtonZone() {
        if (!World.hasWon && Utils.isCoordInZone(Player.spriteObj, Player.redButtonZoneObj)) {
            Player.triggerWin();
        }
    }

    static triggerWin() {
        World.hasWon = true;
        game.physics.world.removeCollider(Player.redButtonUnPressedColliderObj);
        World.redButtonUnPressedLayer.setVisible(false);
        World.redButtonPressedLayer.setVisible(true);

        Player.checkpointNum = 0;
        localStorage.setItem('checkpointNum', '' + Player.checkpointNum);
        Player.setSpawnPoint(true);

        SoundEffect.play('win');
        Help.showMessage('helpMessageWon');
        $('#winHeading').fadeIn(Help.fadeOutMillis);

        setInterval(function () {
            if (Player.playerLightCircleRadius < 20000) {
                Player.playerLightCircleRadius += 50;
            } else {
                clearInterval(this);
            }
        }, 10);

        var confettiCounter = 0;
        setInterval(function () {
            if (confettiCounter < 4) {
                for (var i = 0; i < World.confettiEmittersArr.length; i++) {
                    World.confettiEmittersArr[i].explode(20, Player.spriteObj.x, Player.spriteObj.y - 50);
                }
                confettiCounter++;
            } else {
                clearInterval(this);
            }
        }, 100);
    }

}

Player.spriteObj = null;
