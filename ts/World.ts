class World {

    static mapObj: Phaser.Tilemaps.Tilemap;
    static currentUpdateTickNum: number = 0;
    static nextUpdateTickNum: number    = 0;

    static collideLayer: Phaser.Tilemaps.StaticTilemapLayer;
    static miscLayer: Phaser.Tilemaps.StaticTilemapLayer;
    static animation1Layer: Phaser.Tilemaps.StaticTilemapLayer;
    static animation2Layer: Phaser.Tilemaps.StaticTilemapLayer;
    static waterLayer: Phaser.Tilemaps.StaticTilemapLayer;
    static redButtonPressedLayer: Phaser.Tilemaps.StaticTilemapLayer;
    static redButtonUnPressedLayer: Phaser.Tilemaps.StaticTilemapLayer;

    static hasWon = false;

    static shadowTexture: Phaser.Textures.CanvasTexture;
    static shadowSprite: Phaser.GameObjects.Image;
    static deathEmitter: Phaser.GameObjects.Particles.ParticleEmitter;
    static checkpointEmitter: Phaser.GameObjects.Particles.ParticleEmitter;
    static movement1Emitter: Phaser.GameObjects.Particles.ParticleEmitter;
    static movement2Emitter: Phaser.GameObjects.Particles.ParticleEmitter;
    static confettiEmittersArr: Phaser.GameObjects.Particles.ParticleEmitter[]                                      = [];
    static signsArr: { x: number, y: number, isShowing: boolean, textObj: Phaser.GameObjects.Text, text: string }[] = [
        {'x': null, 'y': null, 'isShowing': false, 'textObj': null, 'text': "No jumping\non the toilet"},
        {'x': null, 'y': null, 'isShowing': false, 'textObj': null, 'text': "Drinking water\nNo swimming"},
        {'x': null, 'y': null, 'isShowing': false, 'textObj': null, 'text': "WARNING:\nSteep cliffs"},
        {'x': null, 'y': null, 'isShowing': false, 'textObj': null, 'text': "Note to self:\nFix bridge"},
        {'x': null, 'y': null, 'isShowing': false, 'textObj': null, 'text': "Dead end\nGo back"},
        {'x': null, 'y': null, 'isShowing': false, 'textObj': null, 'text': "DANGER:\nImminent death"},
        {'x': null, 'y': null, 'isShowing': false, 'textObj': null, 'text': "Note to self:\nBuild zipline"},
        {'x': null, 'y': null, 'isShowing': false, 'textObj': null, 'text': "Construction zone:\nElevator being installed"},
        {'x': null, 'y': null, 'isShowing': false, 'textObj': null, 'text': "Do not press button\n(Frees prisoners)"},
    ];
    static readonly shadowRgb: string                                                                               = 'rgb(0, 0, 0)';

    static createTilemap() {
        World.mapObj       = game.make.tilemap({'key': 'tilemap'});
        const tileset      = World.mapObj.addTilesetImage('Tileset', 'spritesheet', 16, 16, 1, 2);
        World.collideLayer = World.mapObj.createStaticLayer('Collide', tileset, 0, 0);
        World.collideLayer.setScale(Utils.scaleFactorNum);
        World.collideLayer.setCollisionByExclusion([-1]);
        World.collideLayer.depth = Utils.depthsArr['world'];

        World.miscLayer = World.mapObj.createStaticLayer('Misc', tileset, 0, 0);
        World.miscLayer.setScale(Utils.scaleFactorNum);
        World.miscLayer.depth = Utils.depthsArr['Misc'];

        World.animation1Layer = World.mapObj.createStaticLayer('Animation1', tileset, 0, 0);
        World.animation1Layer.setScale(Utils.scaleFactorNum);
        World.animation1Layer.depth = Utils.depthsArr['Animation1'];

        World.animation2Layer = World.mapObj.createStaticLayer('Animation2', tileset, 0, 0);
        World.animation2Layer.setScale(Utils.scaleFactorNum);
        World.animation2Layer.depth = Utils.depthsArr['Animation2'];
        World.animation2Layer.setVisible(false);

        World.waterLayer = World.mapObj.createStaticLayer('Water', tileset, 0, 0);
        World.waterLayer.setScale(Utils.scaleFactorNum);
        World.waterLayer.depth = Utils.depthsArr['Water'];

        World.redButtonPressedLayer = World.mapObj.createStaticLayer('redButtonPressed', tileset, 0, 0);
        World.redButtonPressedLayer.setScale(Utils.scaleFactorNum);
        World.redButtonPressedLayer.setCollisionByExclusion([-1]);
        World.redButtonPressedLayer.depth = Utils.depthsArr['world'];
        World.redButtonPressedLayer.setVisible(false);

        World.redButtonUnPressedLayer = World.mapObj.createStaticLayer('redButtonUnPressed', tileset, 0, 0);
        World.redButtonUnPressedLayer.setScale(Utils.scaleFactorNum);
        World.redButtonUnPressedLayer.setCollisionByExclusion([-1]);
        World.redButtonUnPressedLayer.depth = Utils.depthsArr['Animation1'];

        setInterval(function () {
            if (World.animation1Layer.visible) {
                World.animation1Layer.setVisible(false);
                World.animation2Layer.setVisible(true);
            } else {
                World.animation1Layer.setVisible(true);
                World.animation2Layer.setVisible(false);
            }
        }, 500);

        World.mapObj.findObject('checkPointZones', function (obj: Phaser.GameObjects.Zone) {
            Player.checkPointsArr.push({
                'startX': obj.x * Utils.scaleFactorNum,
                'endX':   (obj.x * Utils.scaleFactorNum) + (obj.width * Utils.scaleFactorNum),
                'startY': obj.y * Utils.scaleFactorNum,
                'endY':   (obj.y * Utils.scaleFactorNum) + (obj.height * Utils.scaleFactorNum),
            });
        });

        World.mapObj.findObject('deathZones', function (obj: Phaser.GameObjects.Zone) {
            Player.deathZonesArr.push({
                'startX': obj.x * Utils.scaleFactorNum,
                'endX':   (obj.x * Utils.scaleFactorNum) + (obj.width * Utils.scaleFactorNum),
                'startY': obj.y * Utils.scaleFactorNum,
                'endY':   (obj.y * Utils.scaleFactorNum) + (obj.height * Utils.scaleFactorNum),
            });
        });

        World.mapObj.findObject('waterZones', function (obj: Phaser.GameObjects.Zone) {
            Player.waterZonesArr.push({
                'startX': obj.x * Utils.scaleFactorNum,
                'endX':   (obj.x * Utils.scaleFactorNum) + (obj.width * Utils.scaleFactorNum),
                'startY': obj.y * Utils.scaleFactorNum,
                'endY':   (obj.y * Utils.scaleFactorNum) + (obj.height * Utils.scaleFactorNum),
            });
        });

        World.mapObj.findObject('crouchHelpZone', function (obj: Phaser.GameObjects.Zone) {
            Player.crouchHelpZone.push({
                'startX': obj.x * Utils.scaleFactorNum,
                'endX':   (obj.x * Utils.scaleFactorNum) + (obj.width * Utils.scaleFactorNum),
                'startY': obj.y * Utils.scaleFactorNum,
                'endY':   (obj.y * Utils.scaleFactorNum) + (obj.height * Utils.scaleFactorNum),
            });
        });

        let signIndexNum = 0;
        World.mapObj.findObject('signs', function (obj: Phaser.GameObjects.Zone) {
            World.signsArr[signIndexNum]['x'] = obj.x * Utils.scaleFactorNum;
            World.signsArr[signIndexNum]['y'] = obj.y * Utils.scaleFactorNum;
            signIndexNum++;
        });

        World.mapObj.findObject('redButtonZone', function (obj: Phaser.GameObjects.Zone) {
            Player.redButtonZoneObj = {
                'startX': obj.x * Utils.scaleFactorNum,
                'endX':   (obj.x * Utils.scaleFactorNum) + (obj.width * Utils.scaleFactorNum),
                'startY': obj.y * Utils.scaleFactorNum,
                'endY':   (obj.y * Utils.scaleFactorNum) + (obj.height * Utils.scaleFactorNum),
            };
        });
    }

    static createParticles() {
        const deathParticleObj = game.add.particles('deathParticle');
        World.deathEmitter     = deathParticleObj.createEmitter({
            'speed':     200,
            'angle':     {'min': 0, 'max': 360},
            'scale':     {'start': 0.5, 'end': 0.1},
            'blendMode': 'NORMAL',
            'lifespan':  200,
            'gravityY':  0,
            'on':        false,
        });
        deathParticleObj.setDepth(Utils.depthsArr['particles']);

        const checkpointParticleObj = game.add.particles('checkpointParticle');
        World.checkpointEmitter     = checkpointParticleObj.createEmitter({
            'speed':     {'min': 300, 'max': 400},
            'angle':     {'min': -60, 'max': -120},
            'scale':     {'start': 0.2, 'end': 0.1},
            'blendMode': 'NORMAL',
            'lifespan':  300,
            'gravityY':  0,
            'on':        false,
        });
        checkpointParticleObj.setDepth(Utils.depthsArr['particles']);

        const movementEmitterSettingsObj = {
            'speed':     0,
            'angle':     {'min': 0, 'max': 360},
            'scale':     {'start': 4, 'end': 1},
            'blendMode': 'NORMAL',
            'lifespan':  500,
            'gravityY':  0,
            'on':        false,
        };
        const movement1ParticleObj       = game.add.particles('movementParticle1');
        World.movement1Emitter           = movement1ParticleObj.createEmitter(movementEmitterSettingsObj);
        movement1ParticleObj.setDepth(Utils.depthsArr['particles']);

        const movement2ParticleObj = game.add.particles('movementParticle2');
        World.movement2Emitter     = movement2ParticleObj.createEmitter(movementEmitterSettingsObj);
        movement2ParticleObj.setDepth(Utils.depthsArr['particles']);

        var confettiParticlesArr = [
            game.add.particles('confetti1Particle'),
            game.add.particles('confetti2Particle'),
            game.add.particles('confetti3Particle'),
            game.add.particles('confetti4Particle'),
            game.add.particles('confetti5Particle'),
        ];
        for (var i = 0; i < confettiParticlesArr.length; i++) {
            confettiParticlesArr[i].setDepth(Utils.depthsArr['particles']);
            var emitterObj = confettiParticlesArr[i].createEmitter({
                'speed':     {'min': 800, 'max': 900},
                'angle':     {'min': -60, 'max': -120},
                'rotate':    {'min': 360, 'max': -360},
                'scale':     2,
                'blendMode': 'NORMAL',
                'lifespan':  2000,
                'gravityY':  1200,
                'on':        false,
            });
            World.confettiEmittersArr.push(emitterObj);
        }
    }

    static createShadow() {
        World.shadowTexture      = game.textures.createCanvas('shadowTexture', window.innerWidth, window.innerHeight);
        World.shadowSprite       = game.add.image(0, 0, 'shadowTexture');
        World.shadowSprite.depth = Utils.depthsArr['light'];
        World.shadowSprite.setOrigin(0);
        World.shadowSprite.setScrollFactor(0); // Fixes to camera
        World.shadowSprite.setBlendMode(Phaser.BlendModes.MULTIPLY);
    }

    static updateShadowTexture() {
        World.currentUpdateTickNum++;
        if (World.currentUpdateTickNum >= World.nextUpdateTickNum) {
            World.shadowTexture.context.fillStyle = World.shadowRgb;
            World.shadowTexture.context.fillRect(0, 0, window.innerWidth, window.innerHeight);
            if (
                Player.spriteObj.x >= game.cameras.main.scrollX - Player.playerLightCircleRadius
                &&
                Player.spriteObj.y >= game.cameras.main.scrollY - Player.playerLightCircleRadius
                &&
                Player.spriteObj.x <= game.cameras.main.scrollX + window.innerWidth + Player.playerLightCircleRadius
                &&
                Player.spriteObj.y <= game.cameras.main.scrollY + window.innerHeight + Player.playerLightCircleRadius
            ) {
                let lightCircleRadius    = Player.playerLightCircleRadius + Utils.randBetween(1, 20);
                const lightCircleObjRelX = Player.spriteObj.x - game.cameras.main.scrollX;
                const lightCircleObjRelY = Player.spriteObj.y - game.cameras.main.scrollY;
                const gradient           = World.shadowTexture.context.createRadialGradient(
                    lightCircleObjRelX, lightCircleObjRelY, 0,
                    lightCircleObjRelX, lightCircleObjRelY, lightCircleRadius,
                );
                gradient.addColorStop(0.0, 'rgba(255, 255, 255, 1.0)');
                gradient.addColorStop(1.0, 'rgba(  0,   0,   0, 0.0)');
                World.shadowTexture.context.beginPath();
                World.shadowTexture.context.fillStyle = gradient;
                World.shadowTexture.context.arc(lightCircleObjRelX, lightCircleObjRelY, lightCircleRadius, 0, Math.PI * 2);
                World.shadowTexture.context.fill();
            }

            World.shadowTexture.refresh();
            World.nextUpdateTickNum += Player.lightTickNum;
        }
    }
}
