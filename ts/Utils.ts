class Utils {

    static readonly scaleFactorNum                            = 4;
    static readonly depthsArr: { [depthKey: string]: number } = {
        'world':      1,
        'light':      2,
        'player':     3,
        'Water':      9,
        'Signs':      9,
        'Misc':       9,
        'Animation1': 9,
        'Animation2': 9,
        'particles':  10,
    };

    // This is a simple function-shortcut to avoid using lengthy document.getElementById.
    static byId(elementId: string): HTMLElement {
        return document.getElementById(elementId);
    }

    static randBetween(min: number, max: number): number {
        return Math.round(Math.random() * (max - min) + min);
    }

    static isCoordInZone(coordObj: Coord, zoneObj: Zone) {
        return (
            coordObj['x'] > zoneObj['startX']
            &&
            coordObj['x'] < zoneObj['endX']
            &&
            coordObj['y'] > zoneObj['startY']
            &&
            coordObj['y'] < zoneObj['endY']
        );
    }

    static distBetweenPoints(x1: number, y1: number, x2: number, y2: number) {
        var dx = x1 - x2;
        var dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    }

    static fadeIn(spriteObj: Phaser.GameObjects.GameObject, durationNum: number, callbackFunc: () => void) {
        game.add.tween({
            targets:    [spriteObj],
            ease:       'Linear',
            duration:   durationNum,
            delay:      0,
            alpha:      {
                getStart: function () {
                    return 0;
                },
                getEnd:   function () {
                    return 1;
                },
            },
            onComplete: function () {
                callbackFunc();
            },
        });
    }

    static fadeOut(spriteObj: Phaser.GameObjects.GameObject, durationNum: number, callbackFunc: () => void) {
        game.add.tween({
            targets:    [spriteObj],
            ease:       'Linear',
            duration:   durationNum,
            delay:      0,
            alpha:      {
                getStart: function () {
                    return 1;
                },
                getEnd:   function () {
                    return 0;
                },
            },
            onComplete: function () {
                callbackFunc();
            },
        });
    }

    static time(): number {
        return new Date().getTime();
    }

}
