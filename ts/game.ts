$(function () {
    setTimeout(function () {
        $('#introHeading').fadeOut(Help.fadeOutMillis);
    }, 2000);
    Help.init();
    Help.showMessage('helpMessageFreeFriends', 2000);
});

let game: Phaser.Scene;

new Phaser.Game({
    type:      Phaser.AUTO,
    parent:    'lite-knight',
    width:     window.innerWidth,
    height:    window.innerHeight,
    // @ts-ignore
    antialias: false,
    physics:   {
        default: 'arcade',
        arcade: {
            tileBias: 24,
            gravity:  {y: 1100},
            debug:    false,
        },
    },
    scene:     {
        preload() {
            game = this;
            game.load.tilemapTiledJSON('tilemap', 'assets/map/lite-knight.json');
            game.load.image('spritesheet', 'assets/map/spritesheet_extruded.png');
            game.load.spritesheet('player', 'assets/player.png', {frameWidth: 16, frameHeight: 16, spacing: 1});
            game.load.image('deathParticle', 'assets/particles/deathParticle.png');
            game.load.image('checkpointParticle', 'assets/particles/checkpointParticle.png');

            game.load.image('movementParticle1', 'assets/particles/movementParticle1.png');
            game.load.image('movementParticle2', 'assets/particles/movementParticle2.png');

            game.load.image('confetti1Particle', 'assets/particles/confetti1Particle.png');
            game.load.image('confetti2Particle', 'assets/particles/confetti2Particle.png');
            game.load.image('confetti3Particle', 'assets/particles/confetti3Particle.png');
            game.load.image('confetti4Particle', 'assets/particles/confetti4Particle.png');
            game.load.image('confetti5Particle', 'assets/particles/confetti5Particle.png');
            SoundEffect.preload();
        },
        create() {
            document.querySelector('canvas').addEventListener('click', function () {
                if (document.activeElement) {
                    (<HTMLInputElement>document.activeElement).blur();
                }
                this.focus();
            });
            window.addEventListener('resize', function () {
                game.scale.resize(window.innerWidth, window.innerHeight);
                game.cameras.main.setSize(window.innerWidth, window.innerHeight);
                World.shadowTexture.setSize(window.innerWidth, window.innerHeight);
            }, false);
            game.cameras.main.setSize(window.innerWidth, window.innerHeight);

            SoundEffect.create();
            // SoundEffect.play('backgroundMusic');
            World.createTilemap();
            World.createShadow();
            Player.init();

            World.createParticles();
        },
        update() {
            Player.update();
            World.updateShadowTexture();
        },
    },
});
